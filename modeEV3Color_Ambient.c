
task main()
{

	SensorType[S3]  = sensorEV3_Color;
	SensorMode[S3]  = modeEV3Color_Ambient;

	while (true)
	{
		// Write the amount of reflected light to the screen between 0 -100
		displayBigTextLine(4, "Ambient: %d", SensorValue[S3]);
		// Wait 20 ms to get 50 readings per second
		sleep(20);
	}
}
