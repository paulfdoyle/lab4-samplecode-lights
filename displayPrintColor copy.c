task main()
{
	int currentColour;
	
	SensorType[S3]  = sensorEV3_Color;
	SensorMode[S3]  = modeEV3Color_Color;
	
	while (true)
	{
		// Colours range from 0 to 7
		currentColour = SensorValue[S3];
		switch(currentColour)
		{
			case 0:  displayCenteredBigTextLine(4, "none"); break;
			case 1:  displayCenteredBigTextLine(4, "black"); break;
			case 2:	displayCenteredBigTextLine(4, "blue"); break;
			case 3:	displayCenteredBigTextLine(4, "green"); break;
			case 4:	displayCenteredBigTextLine(4, "yellow"); break;
			case 5:	displayCenteredBigTextLine(4, "red"); break;
			case 6:	displayCenteredBigTextLine(4, "white"); break;
			case 7:	displayCenteredBigTextLine(4, "brown"); break;
			default: displayCenteredBigTextLine(4, "unknown");
		}
		sleep(200);
	}
}

