task main()
{
	TLegoColors colour = colorNone;
	
	SensorType[S3]  = sensorEV3_Color;
	SensorMode[S3]  = modeEV3Color_Color;
	
	while (true)	{
		colour = getColorName(S3);
		writeDebugStream("Colour detected: ");
		switch(colour){
			case colorNone: writeDebugStreamLine("none"); break;
			case colorBlack: writeDebugStreamLine("black"); break;
			case colorBlue: writeDebugStreamLine("blue"); break;
			case colorGreen: writeDebugStreamLine("green"); break;
			case colorYellow: writeDebugStreamLine("yellow"); break;
			case colorRed: writeDebugStreamLine("red"); break;
			case colorWhite: writeDebugStreamLine("white"); break;
			case colorBrown: writeDebugStreamLine("brown"); break;
			default: writeDebugStreamLine("unknown");
		}
		sleep(200);
	}
}

