task main()
{
	long redValue;
	long greenValue;
	long blueValue;
	SensorType[S3]  = sensorEV3_Color;

	while (true)
	{
		getColorRGB(S3, redValue, greenValue, blueValue);
	  writeDebugStreamLine("Colour detected: %d, %d, %d", redValue, greenValue, blueValue);
		sleep(100);
	}
}
